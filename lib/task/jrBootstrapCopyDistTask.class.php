<?php


class jrBootStrapCopyDist extends sfBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'propel'),
      // add your own options here
    ));

    $this->namespace        = 'jrBootStrap';
    $this->name             = 'copydist';
    $this->briefDescription = 'Copy the compiled files from dist directory';
    $this->detailedDescription = <<<EOF
The [test|INFO] task does things.
Call it with:

  [php symfony test|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    $pluginPath = sfConfig::get("sf_plugins_dir").DIRECTORY_SEPARATOR.'jrBootstrapPlugin';
    $bootstrap  = $pluginPath.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'bootstrap';

    $file = new sfFilesystem();
    $file->mkdirs($pluginPath.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR);
    //$file->copy($bootstrap.DIRECTORY_SEPARATOR.'dist',$pluginPath.DIRECTORY_SEPARATOR.'web',array('override'=>true));
    $file->mirror($bootstrap.DIRECTORY_SEPARATOR.'dist',$pluginPath.DIRECTORY_SEPARATOR.'web',new sfFinder(),array('override'=>true));
  }
}