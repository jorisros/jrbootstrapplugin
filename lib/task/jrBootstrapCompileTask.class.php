<?php

/**
 * Created by JetBrains PhpStorm.
 * User: jorisros
 * Date: 9/28/13
 * Time: 6:28 PM
 * To change this template use File | Settings | File Templates.
 */



class jrBootStrapCompile extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      // add your own options here
    ));

    $this->namespace        = 'jrBootStrap';
    $this->name             = 'compile';
    $this->briefDescription = 'Compiles the less files into css files';
    $this->detailedDescription = <<<EOF
The [test|INFO] task does things.
Call it with:

  [php symfony test|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // add your code here
    BootstapCompile::checkDependencies();

    BootstapCompile::compile();

  }
}

class BootStapCompile
{

  public static function compile()
  {
    $pluginPath = sfConfig::get("sf_plugins_dir").DIRECTORY_SEPARATOR.'jrBootstrapPlugin';
    $bootstrap  = $pluginPath.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'bootstrap';

    $commandline = "cd ".$bootstrap." && grunt dist && cd ".sfConfig::get('sf_root_dir');
    $cmd = CommandlineCMD::run($commandline);
  }

  public static function checkDependencies()
  {
    self::checkNpm();

    self::checkGrunt();

    self::checkLess();

  }

  private static function checkGrunt()
  {
    $cmd = CommandlineCMD::run("grunt -version");
    if(!preg_match('/^grunt-cli/',$cmd))
    {
      throw new sfException("Grunt is required to compile, please install grunt.
        npm install -g grunt-cli

        Dont forget this command:
        npm install
      ");
    }
  }

  private static function checkNpm()
  {
    $cmd = CommandlineCMD::run('which npm');

    if(!file_exists($cmd))
    {
      throw new sfException("NPM is required to compile, please install npm. Goto https://npmjs.org");
    }

  }

  private static function checkLess()
  {
    $cmd = CommandlineCMD::run('which less');

    if(!file_exists($cmd))
    {
      throw new sfException("Less is required to compile, please install less:

        npm install less

      ");
    }
  }
}
class CommandlineCMD
{
  public static function run($command)
  {
   return exec($command);
  }
}